#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	int fd;
	void *ptr;
	size_t len;
	struct stat stats;

	if (argc != 2) {
		return -1;
	}
	if (stat(argv[1], &stats) != 0) {
		perror("load_smtpfile");
		return -1;
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("load_smtpfile");
		return -1;
	}
	ptr = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (ptr == NULL) {
		perror("load_smtpfile");
		return -1;
	}
	len = 0;
	printf("#include <sys/types.h>\n\n");
	printf("size_t rinoo_png_size = %u;\n", (unsigned int) stats.st_size);
	printf("char rinoo_png[] = {\n        ");
	while (len < stats.st_size) {
		printf("0x%02hx", *((unsigned char *) ptr + len));
		len++;
		if (len != stats.st_size) {
			if (len % 10 == 0) {
				printf(",\n        ");
			} else {
				printf(", ");
			}
		}
	}
	printf("\n};\n");
	munmap(ptr, stats.st_size);
	close(fd);
	return 0;
}
