/**
 * @file   module.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Thu Nov 25 12:27:47 2010
 *
 * @brief  Global header file which includes module dependecies
 *
 *
 */

#ifndef		RSC_MODULE_GLOBAL_H_
# define	RSC_MODULE_GLOBAL_H_

# include	"global/util.h"

#endif		/* !RSC_MODULE_GLOBAL_H_ */
