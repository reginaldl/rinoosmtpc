/**
 * @file   util.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Thu Nov 25 12:28:45 2010
 *
 * @brief  Header file which declares util functions.
 *
 *
 */

#ifndef		RSC_GLOBAL_UTIL_H_
# define	RSC_GLOBAL_UTIL_H_

#define		rsc_log(config, level, format, ...)			\
	do {								\
		if (config->log >= level) {				\
			rinoo_log("rsc " format, ## __VA_ARGS__);	\
		}							\
	} while (0)


int rsc_getip(char *hostname, t_ip *ip);

#endif		/* RSC_GLOBAL_UTIL_H_ */
