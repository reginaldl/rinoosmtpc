/**
 * @file   email.h
 * @author Reginald LIPS <reginald.l@gmail.com>
 * @date   Mon Dec 10 11:43:55 2012
 *
 * @brief  Email definitions
 *
 *
 */

#ifndef		RSC_SMTP_EMAIL_H_
# define	RSC_SMTP_EMAIL_H_

typedef enum e_email_data_type {
	RSC_EMAIL_DATA_NONE = 0,
	RSC_EMAIL_DATA_BUFFER,
	RSC_EMAIL_DATA_FILE
} t_email_data_type;

typedef struct s_email_data_file {
	int fd;
	t_buffer map;
} t_email_data_file;

typedef struct s_email_data {
	t_email_data_type type;
	union {
		t_buffer *buffer;
		t_email_data_file *file;
	};
} t_email_data;

typedef struct s_email {
	char *from;
	t_vector *recipients;
	t_email_data data;
	t_session *session;
} t_email;

t_email *rinoo_email(void);
void rinoo_email_destroy(t_email *email);
int rinoo_email_session_set(t_email *email, t_session *session);
int rinoo_email_from_set(t_email *email, const char *from);
const char *rinoo_email_from_get(t_email *email);
int rinoo_email_rcpt_add(t_email *email, const char *rcpt);
size_t rinoo_email_rcpt_count(t_email *email);
const char *rinoo_email_rcpt_get(t_email *email, uint32_t i);
int rinoo_email_data_set_buffer(t_email *email, const char *data);
int rinoo_email_data_set_file(t_email *email, const char *path);
t_buffer *rinoo_email_data_get(t_email *email);
void rinoo_email_data_destroy(t_email *email);

#endif		/* !RSC_SMTP_EMAIL_H_ */
