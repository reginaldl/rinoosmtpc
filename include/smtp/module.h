/**
 * @file   module.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Thu Nov 25 12:27:47 2010
 *
 * @brief  SMTP module header
 *
 *
 */

#ifndef		RSC_MODULE_SMTP_H_
# define	RSC_MODULE_SMTP_H_

# include	"smtp/session.h"
# include	"smtp/email.h"
# include	"smtp/smtp.h"

#endif		/* !RSC_MODULE_SMTP_H_ */
