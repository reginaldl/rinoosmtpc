/**
 * @file   smtp.h
 * @author Reginald LIPS <reginald.l@gmail.com>
 * @date   Mon Dec 10 11:45:59 2012
 *
 * @brief  SMTP function declarations
 *
 *
 */

#ifndef		RSC_SMTP_SMTP_H_
# define	RSC_SMTP_SMTP_H_

int rinoo_smtp_start(t_email *email);
int rinoo_smtp_send(t_email *email);
int rinoo_smtp_end(t_email *email);

#endif		/* !RSC_SMTP_SMTP_H_ */
