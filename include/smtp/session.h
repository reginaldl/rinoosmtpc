/**
 * @file   email.h
 * @author Reginald LIPS <reginald.l@gmail.com>
 * @date   Fri May  2 20:30:00 2014
 *
 * @brief  SMTP session structure definitions
 *
 *
 */

#ifndef		RSC_SMTP_SESSION_H_
# define	RSC_SMTP_SESSION_H_

struct s_email;

typedef struct s_session {
	const char *helo;
	t_buffer *read;
	t_buffer *write;
	t_socket *socket;
	struct s_email *email;
} t_session;

#endif		/* !RSC_SMTP_SESSION_H_ */

