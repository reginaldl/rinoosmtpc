/**
 * @file   config.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Thu Nov 25 11:44:21 2010
 *
 * @brief  Header file for RiNOO SMTP Client configuration
 *
 *
 */

#ifndef		RSC_CONFIG_H_
# define	RSC_CONFIG_H_

typedef enum e_rscconfig_mode {
	RSC_MODE_NONE,
	RSC_MODE_FLOOD,
	RSC_MODE_SECOND
} t_rscconfig_mode;

typedef enum e_rscconfig_corpus {
	RSC_CORPUS_NONE,
	RSC_CORPUS_FILES,
	RSC_CORPUS_MEMORY
} t_rscconfig_corpus;

typedef enum e_rscconfig_log {
	RSC_LOG_NONE,
	RSC_LOG_REPORT,
	RSC_LOG_ERROR,
	RSC_LOG_NORMAL
} t_rscconfig_log;

typedef struct s_rscconfig_connection {
	t_ip ip;
	uint32_t port;
	uint32_t timeout;
	uint32_t sessions;
	char *server;
} t_rscconfig_connection;

typedef struct s_rscconfig_session {
	uint32_t nbmsg;
	char *helo;
} t_rscconfig_session;

typedef struct s_rscconfig_counter {
	uint32_t sessions;
	uint32_t sent;
	uint32_t failed;
	uint32_t speed;
	double rate;
} t_rscconfig_counter;

typedef struct s_rscconfig_stat {
	t_rscconfig_counter all;
	t_rscconfig_counter *thread;
} t_rscconfig_stat;

typedef struct s_rscconfig {
	bool gui;
	uint16_t threads;
	t_email *email;
	t_rscconfig_log log;
	t_rscconfig_mode mode;
	t_rscconfig_stat stats;
	t_rscconfig_corpus corpus;
	t_rscconfig_session session;
	t_rscconfig_connection connection;
} t_rscconfig;

t_rscconfig	*rsc_config_create(void);
void		rsc_config_destroy(t_rscconfig *config);
int		rsc_config_parse_options(int argc, char **argv, t_rscconfig *config);

#endif		/* !RSC_CONFIG_H_ */
