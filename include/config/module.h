/**
 * @file   module.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2011
 * @date   Thu Nov 25 11:46:20 2010
 *
 * @brief  Global header file which includes modules dependencies.
 *
 *
 */

#ifndef		RSC_MODULE_CONFIG_H_
# define	RSC_MODULE_CONFIG_H_

# include	"config/config.h"

#endif		/* !RSC_MODULE_CONFIG_H_ */
