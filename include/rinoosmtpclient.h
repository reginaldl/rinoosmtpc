/**
 * @file   rsc.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2011
 * @date   Thu Nov 25 11:47:51 2010
 *
 * @brief  Global header file for RiNOO SMTP Client.
 *
 *
 */

#ifndef		RINOOSMTPCLIENT_H_
# define	RINOOSMTPCLIENT_H_

# include	<unistd.h>
# include	<getopt.h>
# include	<sys/types.h>
# include	<sys/socket.h>
# include	<netdb.h>
# include	<rinoo/rinoo.h>

# include	"global/module.h"
# include	"smtp/module.h"
# include	"config/module.h"

# define	RSC_DEFAULT_SERVER	"localhost"
# define	RSC_DEFAULT_PORT	25
# define	RSC_DEFAULT_TIMEOUT	30
# define	RSC_DEFAULT_HELO	"rinoo"
# define	RSC_DEFAULT_FROM	"<test@test.com>"
# define	RSC_DEFAULT_RCPT	"<test@test.com>"
# define	RSC_DEFAULT_DATA	"Subject: Test message\r\n\r\nThis is a test message from Rinoo SMTP Sender."

#endif		/* !RINOOSMTPCLIENT_H_ */
