/**
 * @file   smtp.c
 * @author Reginald LIPS <reginald.l@gmail.com>
 * @date   Fri May  2 20:36:28 2014
 *
 * @brief  SMTP client stack
 *
 *
 */

#include "rinoosmtpclient.h"

t_email *rinoo_email(void)
{
	t_email *email;

	email = calloc(1, sizeof(*email));
	if (email == NULL) {
		return NULL;
	}
	email->recipients = calloc(1, sizeof(*email->recipients));
	if (email->recipients == NULL) {
		free(email);
		return NULL;
	}
	return email;
}

void rinoo_email_destroy(t_email *email)
{
	uint32_t i;

	if (email == NULL) {
		return;
	}
	email->session = NULL;
	if (email->from != NULL) {
		free(email->from);
	}
	for (i = 0; i < vector_size(email->recipients); i++) {
		free(vector_get(email->recipients, i));
	}
	vector_destroy(email->recipients);
	free(email->recipients);
	rinoo_email_data_destroy(email);
	free(email);
}

int rinoo_email_session_set(t_email *email, t_session *session)
{
	if (email == NULL || session == NULL) {
		return -1;
	}
	email->session = session;
	return 0;
}

int rinoo_email_from_set(t_email *email, const char *from)
{
	char *fromd;

	if (email == NULL || from == NULL) {
		return -1;
	}
	fromd = strdup(from);
	if (fromd == NULL) {
		return -1;
	}
	if (email->from != NULL) {
		free(email->from);
	}
	email->from = fromd;
	return 0;
}

const char *rinoo_email_from_get(t_email *email)
{
	if (email == NULL) {
		return NULL;
	}
	return email->from;
}

int rinoo_email_rcpt_add(t_email *email, const char *rcpt)
{
	char *rcptd;

	if (email == NULL || rcpt == NULL) {
		return -1;
	}
	rcptd = strdup(rcpt);
	if (rcptd == NULL) {
		return -1;
	}
	if (vector_add(email->recipients, rcptd) != 0) {
		free(rcptd);
		return -1;
	}
	return 0;
}

size_t rinoo_email_rcpt_count(t_email *email)
{
	if (email == NULL) {
		return -1;
	}
	return vector_size(email->recipients);
}

const char *rinoo_email_rcpt_get(t_email *email, uint32_t i)
{
	if (email == NULL) {
		return NULL;
	}
	return vector_get(email->recipients, i);
}

int rinoo_email_data_set_buffer(t_email *email, const char *data)
{
	if (email == NULL || data == NULL) {
		return -1;
	}
	rinoo_email_data_destroy(email);
	email->data.buffer = buffer_create(NULL);
	if (email->data.buffer == NULL) {
		return -1;
	}
	email->data.type = RSC_EMAIL_DATA_BUFFER;
	buffer_addstr(email->data.buffer, data);
	return 0;
}

int rinoo_email_data_set_file(t_email *email, const char *path)
{
	void *ptr;
	struct stat stats;

	if (email == NULL || path == NULL) {
		return -1;
	}
	rinoo_email_data_destroy(email);
	if (stat(path, &stats) != 0) {
		goto set_file_error;
	}
	email->data.file = calloc(1, sizeof(*email->data.file));
	if (email->data.file == NULL) {
		goto set_file_error;
	}
	email->data.file->fd = open(path, O_RDONLY);
	if (email->data.file->fd < 0) {
		goto set_file_error;
	}
	ptr = mmap(NULL, stats.st_size, PROT_READ, MAP_PRIVATE, email->data.file->fd, 0);
	if (ptr == NULL) {
		goto set_file_error;
	}
	buffer_static(&email->data.file->map, ptr, stats.st_size);
	email->data.type = RSC_EMAIL_DATA_FILE;
	return 0;
set_file_error:
	rinoo_email_data_destroy(email);
	return -1;
}

t_buffer *rinoo_email_data_get(t_email *email)
{
	if (email == NULL) {
		return NULL;
	}
	switch (email->data.type) {
		case RSC_EMAIL_DATA_NONE:
			break;
		case RSC_EMAIL_DATA_BUFFER:
			return email->data.buffer;
		case RSC_EMAIL_DATA_FILE:
			return &email->data.file->map;
	}
	return NULL;
}

void rinoo_email_data_destroy(t_email *email)
{
	if (email == NULL) {
		return;
	}
	switch (email->data.type) {
		case RSC_EMAIL_DATA_NONE:
			break;
		case RSC_EMAIL_DATA_BUFFER:
			if (email->data.buffer != NULL) {
				buffer_destroy(email->data.buffer);
				email->data.buffer = NULL;
			}
			break;
		case RSC_EMAIL_DATA_FILE:
			if (email->data.file != NULL) {
				if (buffer_ptr(&email->data.file->map) != NULL) {
					munmap(buffer_ptr(&email->data.file->map), buffer_size(&email->data.file->map));
				}
				if (email->data.file->fd > 0) {
					close(email->data.file->fd);
				}
				free(email->data.file);
				email->data.file = NULL;
			}
			break;
	}
	email->data.type = RSC_EMAIL_DATA_NONE;
}
