/**
 * @file   smtp.c
 * @author Reginald LIPS <reginald.l@gmail.com>
 * @date   Mon Dec 10 11:16:42 2012
 *
 * @brief  SMTP client stack
 *
 *
 */

#include "rinoosmtpclient.h"

extern t_rscconfig *config;

static int rinoo_smtp_response_get(t_email *email, int expected)
{
	int code;
	ssize_t size;

	buffer_erase(email->session->read, buffer_size(email->session->read));
	size = rinoo_socket_readline(email->session->socket, email->session->read, "\n", 1024);
	if (size < 0) {
		return -1;
	}
	code = buffer_tolong(email->session->read, NULL, 10);
	if (code != expected) {
		switch (code) {
		case 400 ... 499:
		case 500 ... 599:
			rsc_log(config, RSC_LOG_ERROR, "<- %.*s", buffer_size(email->session->read), buffer_ptr(email->session->read));
			return code;
		default:
			return -1;
		}
	}
	rsc_log(config, RSC_LOG_NORMAL, "<- %.*s", size, buffer_ptr(email->session->read));
	return code;
}

static int rinoo_smtp_command_send(t_email *email, int expected, const char *format, ...)
{
	int code;
	va_list ap;

	buffer_erase(email->session->write, buffer_size(email->session->write));
	va_start(ap, format);
	buffer_vprint(email->session->write, format, ap);
	va_end(ap);
	rsc_log(config, RSC_LOG_NORMAL, "-> %.*s", buffer_size(email->session->write), buffer_ptr(email->session->write));
	if (rinoo_socket_writeb(email->session->socket, email->session->write) <= 0) {
		rsc_log(config, RSC_LOG_ERROR, "Couldn't send command: %.*s", buffer_size(email->session->write), buffer_ptr(email->session->write));
		return -1;
	}
	code = rinoo_smtp_response_get(email, expected);
	if (code < 0) {
		rsc_log(config, RSC_LOG_ERROR, "%.*s - Failed.", buffer_size(email->session->write), buffer_ptr(email->session->write));
		return -1;
	}
	return code;
}

int rinoo_smtp_start(t_email *email)
{
	if (rinoo_smtp_response_get(email, 220) != 220) {
		rsc_log(config, RSC_LOG_ERROR, "Banner failed (%s).", strerror(errno));
		return -1;
	}
	if (rinoo_smtp_command_send(email, 250, "HELO %s\r\n", email->session->helo) != 250) {
		return -1;
	}
	return 0;
}

int rinoo_smtp_send(t_email *email)
{
	size_t i;
	t_buffer eom;
	t_buffer *data[2];

	if (rinoo_smtp_command_send(email, 250, "MAIL FROM: %s\r\n", rinoo_email_from_get(email)) != 250) {
		return -1;
	}
	for (i = 0; i < rinoo_email_rcpt_count(email); i++) {
		if (rinoo_smtp_command_send(email, 250, "RCPT TO: %s\r\n", rinoo_email_rcpt_get(email, i)) != 250) {
			return -1;
		}
	}
	if (rinoo_smtp_command_send(email, 354, "DATA\r\n") != 354) {
		return -1;
	}
	buffer_static(&eom, "\r\n.\r\n", 5);
	data[0] = rinoo_email_data_get(email);
	data[1] = &eom;
	rsc_log(config, RSC_LOG_NORMAL, "-> %.*s\r\n.\r\n", buffer_size(data[0]), buffer_ptr(data[0]));
	if (rinoo_socket_writev(email->session->socket, data, 2) <= 0) {
		rsc_log(config, RSC_LOG_ERROR, "Couldn't send message content.");
		return -1;
	}
	if (rinoo_smtp_response_get(email, 250) != 250) {
		return -1;
	}
	return 0;
}

int rinoo_smtp_end(t_email *email)
{
	rsc_log(config, RSC_LOG_NORMAL, "-> QUIT");
	if (rinoo_socket_write(email->session->socket, "QUIT\r\n", 6) <= 0) {
		return -1;
	}
	return 0;
}
