function refresh()
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState != 4 || xmlhttp.status != 200) {
	    return;
	}
	var data = JSON.parse(xmlhttp.responseText);
	for (var i = 0; i < data.length; i++) {
	    var span = document.getElementById(data[i].k);
	    span.innerHTML = data[i].v;
	}
    };
    xmlhttp.open("GET", "/info", true);
    xmlhttp.send();
    setTimeout(refresh, 1000);
}
