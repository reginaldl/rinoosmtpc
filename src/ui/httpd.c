/**
 * @file   httpd.c
 * @author Reginald Lips <reginald.l@gmail.com> - Copyright 2012
 * @date   Tue Oct 23 21:42:37 2012
 *
 * @brief  RiNOO SMTP Client httpd
 *
 *
 */

#include "rinoosmtpclient.h"

static int rsc_ui_logo(t_http *http, t_http_route *route);
static int rsc_ui_info(t_http *http, t_http_route *route);
static int rsc_ui_stop(t_http *http, t_http_route *route);

extern size_t rinoo_png_size;
extern char rinoo_png[];
extern char rinoo_page[];
extern char rinoo_js[];
extern t_rscconfig *config;
extern t_sched *sched;

t_http_route routes[] = {
	{ "/", 200, RINOO_HTTP_ROUTE_STATIC, .content = rinoo_page },
	{ "/rinoo.png", 200, RINOO_HTTP_ROUTE_FUNC, .func = rsc_ui_logo },
	{ "/ajax.js", 200, RINOO_HTTP_ROUTE_STATIC, .content = rinoo_js },
	{ "/info", 200, RINOO_HTTP_ROUTE_FUNC, .func =  rsc_ui_info },
	{ "/stop", 200, RINOO_HTTP_ROUTE_FUNC, .func =  rsc_ui_stop },
	{ NULL, 302, RINOO_HTTP_ROUTE_REDIRECT, .location = "/" }
};

static int rsc_ui_logo(t_http *http, t_http_route *unused(route))
{
	t_buffer body;

	http->response.code = 200;
	rinoo_http_header_set(&http->response.headers, "Content-Type", "image/png");
	buffer_static(&body, rinoo_png, rinoo_png_size);
	return rinoo_http_response_send(http, &body);
}

static int rsc_ui_info(t_http *http, t_http_route *unused(route))
{
	t_buffer *body;

	body = buffer_create(NULL);
	if (body == NULL) {
		return -1;
	}
	http->response.code = 200;
	rinoo_http_header_set(&http->response.headers, "Content-Type", "text/html");
	buffer_add(body, "[ ", 2);
	buffer_print(body, "{ \"k\": \"host\", \"v\": \"%s\" }, ", config->connection.server);
	buffer_print(body, "{ \"k\": \"port\", \"v\": \"%d\" }, ", config->connection.port);
	buffer_print(body, "{ \"k\": \"timeout\", \"v\": \"%ds\" }, ", config->connection.timeout / 1000);
	buffer_print(body, "{ \"k\": \"nbsessions\", \"v\": \"%d\" }, ", config->connection.sessions);
	buffer_print(body, "{ \"k\": \"nbmails\", \"v\": \"%d\" }, ", config->session.nbmsg);
	buffer_print(body, "{ \"k\": \"sessions\", \"v\": \"%d\" }, ", config->stats.all.sessions);
	buffer_print(body, "{ \"k\": \"sent\", \"v\": \"%d\" }, ", config->stats.all.sent);
	buffer_print(body, "{ \"k\": \"failed\", \"v\": \"%d\" }, ", config->stats.all.failed);
	buffer_print(body, "{ \"k\": \"speed\", \"v\": \"%d msg/s\" }, ", config->stats.all.speed);
	buffer_print(body, "{ \"k\": \"rate\", \"v\": \"%d\" } ", 0);

	buffer_add(body, " ]", 2);
	if (rinoo_http_response_send(http, body) != 0) {
		buffer_destroy(body);
		return -1;
	}
	buffer_destroy(body);
	return 0;
}

static int rsc_ui_stop(t_http *unused(http), t_http_route *unused(route))
{
	rinoo_sched_stop(sched);
	return 0;
}

int rsc_ui_start(t_sched *sched)
{
	return rinoo_http_easy_server(sched, 0, 4225, routes, sizeof(routes) / sizeof(*routes));
}
