/**
 * @file   config.c
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Wed Nov 24 14:41:53 2010
 *
 * @brief  Config function declarations
 *
 *
 */

#include	"rinoosmtpclient.h"

t_rscconfig config;

static struct option long_options[] = {
	/* Connection */
	{"server", 1, 0, 's'},
	{"port", 1, 0, 'p'},
	{"sessions", 1, 0, 'n'},
	{"timeout", 1, 0, 0},
	/* SMTP */
	{"helo", 1, 0, 0},
	{"from", 1, 0, 'f'},
	{"to", 1, 0, 't'},
	{"count", 1, 0, 'c'},
	{"mail", 1, 0, 'm'},
	{"data", 1, 0, 'd'},
	/* Advanced */
	{"flood", 0, 0, 0},
	{"second", 0, 0, 0},
	{"corpus", 1, 0, 0},
	{"mcorpus", 1, 0, 0},
	{"threads", 1, 0, 0},
	{"gui", 0, 0, 'g'},
	/* Handy */
	{"log", 1, 0, 'l'},
	{"quiet", 0, 0, 'q'},
	{"version", 0, 0, 'v'},
	{"help", 0, 0, 'h'},
	{0, 0, 0, 0}
};


static void usage(char *path)
{
	printf("Usage: %s [options]\n\n"
	       "Connection options:\n"
	       "\t-s, --server <server>\n\t\tSMTP server to which to connect.\n"
	       "\t-p, --port <port>\n\t\tPort of the SMTP server to which to connect.\n"
	       "\t-n, --sessions <number>\n\t\tNumber of concurrent sessions.\n"
	       "\t--timeout <number>\n\t\tConnection timeout in seconds. 0 means no timeout.\n"
	       "\nSMTP options:\n"
	       "\t--helo <string>\n\t\tSet the helo string.\n"
	       "\t-f, --from <from>\n\t\tSets the mail from.\n"
	       "\t-t, --to <to>\n\t\tSets the recipient.\n"
	       "\t-c, --count <number>\n\t\tNumber of emails per sessions.\n"
	       "\t-m, --mail <file>\n\t\tLoads a message from a file.\n"
	       "\t-d, --data <data>\n\t\tSpecify message content.\n"
	       "\nAdvanced options:\n"
	       "\t--flood\n\t\tRe-opens connections after closing.\n"
	       "\t--second\n\t\tKeeps opening connections every second.\n"
	       "\t--corpus <dir>\n\t\tLoad mail corpus.\n"
	       "\t--mcorpus <dir>\n\t\tLoad entire mail corpus into memory.\n"
	       "\t--threads <number>\n\t\tNumber of threads to be used.\n"
	       "\t-g, --gui\n\t\tStarts the web gui.\n"
	       "\nHandy options:\n"
	       "\t-l, --log <number>\n\t\tEnable a specific log level: 0 no output (same as -q), 1 reporting, 2 errors only, 3 all logs (default).\n"
	       "\t-q, --quiet\n\t\tDisable output.\n"
	       "\t-v, --version\n\t\tPrints rinoosmtpc version information.\n"
	       "\t-h, --help\n\t\tDisplays this help.\n",
	       path);
}

t_rscconfig *rsc_config_create(void)
{
	t_rscconfig *config;

	config = calloc(1, sizeof(*config));
	if (config == NULL) {
		return NULL;
	}
	config->threads = 1;
	config->log = RSC_LOG_NORMAL;
	config->email = rinoo_email();
	if (config->email == NULL) {
		free(config);
		return NULL;
	}
	config->connection.ip.v4.sin_family = AF_INET;
	config->connection.port = RSC_DEFAULT_PORT;
	config->connection.sessions = 1;
	config->connection.timeout = RSC_DEFAULT_TIMEOUT * 1000;
	config->session.nbmsg = 1;
	return config;
}

void rsc_config_set_defaults(t_rscconfig *config)
{
	if (config->connection.server == NULL) {
		config->connection.server = strdup(RSC_DEFAULT_SERVER);
	}
	if (config->session.helo == NULL) {
		config->session.helo = strdup(RSC_DEFAULT_HELO);
	}
	if (rinoo_email_from_get(config->email) == NULL) {
		rinoo_email_from_set(config->email, RSC_DEFAULT_FROM);
	}
	if (rinoo_email_rcpt_count(config->email) == 0) {
		rinoo_email_rcpt_add(config->email, RSC_DEFAULT_RCPT);
	}
	if (rinoo_email_data_get(config->email) == NULL) {
		rinoo_email_data_set_buffer(config->email, RSC_DEFAULT_DATA);
	}
}

void rsc_config_destroy(t_rscconfig *config)
{
	if (config->stats.thread != NULL) {
		free(config->stats.thread);
	}
	if (config->connection.server != NULL) {
		free(config->connection.server);
	}
	if (config->session.helo != NULL) {
		free(config->session.helo);
	}
	rinoo_email_destroy(config->email);
	free(config);
}

int rsc_config_parse_options(int argc, char **argv, t_rscconfig *config)
{
	int cur;
	int option_index = 0;

	while ((cur = getopt_long(argc, argv, "s:p:f:t:m:d:n:s:c:l:ghqv", long_options, &option_index)) != -1) {
		switch (cur) {
		case 0:
			if (strcmp(long_options[option_index].name, "helo") == 0) {
				config->session.helo = strdup(optarg);
			} else if (strcmp(long_options[option_index].name, "flood") == 0) {
				config->mode = RSC_MODE_FLOOD;
			} else if (strcmp(long_options[option_index].name, "second") == 0) {
				config->mode = RSC_MODE_SECOND;
			} else if (strcmp(long_options[option_index].name, "corpus") == 0) {
				config->corpus = RSC_CORPUS_FILES;
				/* if (rsc_fs_corpus_open(config, optarg) != 0) { */
				/* 	return -1; */
				/* } */
			} else if (strcmp(long_options[option_index].name, "mcorpus") == 0) {
				config->corpus = RSC_CORPUS_MEMORY;
				/* if (rsc_fs_corpus_open(config, optarg) != 0) { */
				/* 	return -1; */
				/* } */
			} else if (strcmp(long_options[option_index].name, "timeout") == 0) {
				config->connection.timeout = atoi(optarg);
			} else if (strcmp(long_options[option_index].name, "threads") == 0) {
				config->threads = atoi(optarg);
				if (config->threads > 10000) {
					return -1;
				}
			}
			break;
		case 's':
			config->connection.server = strdup(optarg);
			if (rsc_getip(config->connection.server, &config->connection.ip) != 0) {
				return -1;
			}
			break;
		case 'p':
			config->connection.port = atoi(optarg);
			break;
		case 'f':
			rinoo_email_from_set(config->email, optarg);
			break;
		case 't':
			rinoo_email_rcpt_add(config->email, optarg);
			break;
		case 'm':
			if (rinoo_email_data_set_file(config->email, optarg) != 0) {
				return -1;
			}
			break;
		case 'd':
			rinoo_email_data_set_buffer(config->email, optarg);
			break;
		case 'c':
			config->session.nbmsg = atoi(optarg);
			break;
		case 'n':
			config->connection.sessions = atoi(optarg);
			break;
		case 'g':
			config->gui = true;
			break;
		case 'l':
			switch (atoi(optarg)) {
			case RSC_LOG_NONE:
				config->log = RSC_LOG_NONE;
				break;
			case RSC_LOG_REPORT:
				config->log = RSC_LOG_REPORT;
				break;
			case RSC_LOG_ERROR:
				config->log = RSC_LOG_ERROR;
				break;
			case RSC_LOG_NORMAL:
				config->log = RSC_LOG_NORMAL;
				break;
			default:
				return -1;
			}
			break;
		case 'q':
			config->log = RSC_LOG_NONE;
			break;
		case 'v':
			printf("RiNOO SMTP Client v%s\n", VERSION);
			return -1;
		case 'h':
			usage(argv[0]);
			return -1;
		default:
			return -1;
		}
	}
	config->stats.thread = calloc(config->threads, sizeof(*config->stats.thread));
	if (config->stats.thread == NULL) {
		return -1;
	}
	rsc_config_set_defaults(config);
	return 0;
}
