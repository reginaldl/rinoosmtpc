/**
 * @file   rsc.c
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2011
 * @date   Thu Nov 25 11:27:38 2010
 *
 * @brief  RiNOO SMTP Client
 *
 *
 */

#include	"rinoosmtpclient.h"

t_rscconfig *config;
t_sched *sched;

static int thread_counter = 0;

int rsc_ui_start(t_sched *sched);

void sig_handler(int unused(signal))
{
	rsc_log(config, RSC_LOG_NORMAL, "Exiting...");
	rinoo_sched_stop(sched);
}

void rsc_start_client(void *client_sched)
{
	bool redo;
	uint32_t i;
	t_email email;
	t_session session = { 0 };

	redo = true;
	email = *config->email;
	session.email = &email;
	session.helo = config->session.helo;
	session.read = buffer_create(NULL);
	if (session.read == NULL) {
		goto send_error;
	}
	session.write = buffer_create(NULL);
	if (session.write == NULL) {
		goto send_error;
	}
	rinoo_email_session_set(&email, &session);
	while (redo) {
		session.socket = rinoo_tcp_client(client_sched, &config->connection.ip, config->connection.port, config->connection.timeout * 1000);
		if (session.socket == NULL) {
			rinoo_log("Error: %s", strerror(errno));
			config->stats.thread[rinoo_sched_self()->id].failed++;
			rsc_log(config, RSC_LOG_ERROR, "Couldn't create socket to %s:%d", config->connection.server, config->connection.port);
			goto send_error;
		}
		config->stats.thread[rinoo_sched_self()->id].sessions++;
		if (rinoo_smtp_start(&email) != 0) {
			goto send_error;
		}
		for (i = 0; i < config->session.nbmsg; i++) {
			if (rinoo_smtp_send(&email) != 0) {
				goto send_error;
			}
			config->stats.thread[rinoo_sched_self()->id].sent++;
		}
		if (rinoo_smtp_end(&email) != 0) {
			goto send_error;
		}
		rinoo_socket_destroy(session.socket);
		session.socket = NULL;
		config->stats.thread[rinoo_sched_self()->id].sessions--;
		if (config->mode == RSC_MODE_SECOND) {
			if (rinoo_task_wait(client_sched, 1000) != 0) {
				goto send_error;
			}
		} else if (config->mode != RSC_MODE_FLOOD) {
			redo = false;
		}
	}
send_error:
	if (session.read != NULL) {
		buffer_destroy(session.read);
	}
	if (session.write != NULL) {
		buffer_destroy(session.write);
	}
	if (session.socket != NULL) {
		config->stats.thread[rinoo_sched_self()->id].sessions--;
		config->stats.thread[rinoo_sched_self()->id].failed++;
		rinoo_socket_destroy(session.socket);
	}
	if (__atomic_fetch_sub(&thread_counter, 1, __ATOMIC_SEQ_CST) == 1) {
		sched->stop = true;
	}
}

void rsc_stats(void)
{
	int i;
	static uint32_t sent = 0;

	memset(&config->stats.all, 0, sizeof(config->stats.all));
	for (i = 0; i < config->threads; i++) {
		config->stats.all.sessions += config->stats.thread[i].sessions;
		config->stats.all.sent += config->stats.thread[i].sent;
		config->stats.all.failed += config->stats.thread[i].failed;
	}
	config->stats.all.speed = config->stats.all.sent - sent;
	sent = config->stats.all.sent;
}

void rsc_stat_aggregate(void *unused(arg))
{
	while (rinoo_task_wait(sched, 1000) == 0) {
		rsc_stats();
	}
}

int rsc_start(t_sched *sched)
{
	uint16_t i;
	uint32_t j;
	t_sched *cur;

	if (config->threads > 1) {
		rinoo_spawn(sched, config->threads - 1);
	}
	for (i = 0; i < config->threads; i++) {
		cur = rinoo_spawn_get(sched, i);
		for (j = 0; j < config->connection.sessions; j++) {
			if (rinoo_task_start(cur, rsc_start_client, cur) != 0) {
				return -1;
			}
			thread_counter++;
		}
	}
	rinoo_task_start(sched, rsc_stat_aggregate, NULL);
	return 0;
}

int main(int argc, char **argv)
{
	sched = rinoo_sched();
	if (sched == NULL) {
		return -1;
	}
	config = rsc_config_create();
	if (config == NULL) {
		rinoo_log("could not create configuration");
		rinoo_sched_destroy(sched);
		return -1;
	}
	if (rsc_config_parse_options(argc, argv, config) != 0) {
		rsc_config_destroy(config);
		rinoo_sched_destroy(sched);
		return -1;
	}
	if (signal(SIGINT, sig_handler) == SIG_ERR) {
		rsc_config_destroy(config);
		rinoo_sched_destroy(sched);
		return -1;
	}
	if (config->gui) {
		if (rsc_ui_start(sched) != 0) {
			rinoo_log("could not start web server");
			rsc_config_destroy(config);
			rinoo_sched_destroy(sched);
			return -1;
		}
		thread_counter++;
	}
	if (rsc_start(sched) != 0) {
		rinoo_log("could not create threads");
		rsc_config_destroy(config);
		rinoo_sched_destroy(sched);
		return -1;
	}
	rinoo_sched_loop(sched);
	rinoo_sched_destroy(sched);
	rsc_stats();
	rsc_log(config, RSC_LOG_REPORT, "Report - Sent: %d Failed: %d Total: %d", config->stats.all.sent, config->stats.all.failed, config->stats.all.sent + config->stats.all.failed);
	rsc_config_destroy(config);
	return 0;
}
