/**
 * @file   util.c
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2012
 * @date   Thu Nov 25 12:16:50 2010
 *
 * @brief  Util functions for RiNOO SMTP client
 *
 *
 */

#include "rinoosmtpclient.h"

/**
 * Resolv an hostname to an IP address.
 *
 * @param hostname Pointer to the hostname to convert.
 * @param ip Pointer to an ip structure where to store the result.
 *
 * @return 0 on success, or -1 if an error occurs.
 */
int rsc_getip(char *hostname, t_ip *ip)
{
	int res;
	struct addrinfo hints;
	struct addrinfo *infos;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = 0;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;
	res = getaddrinfo(hostname, NULL, &hints, &infos);
	if (res != 0) {
		rinoo_log("%s", gai_strerror(res));
		return -1;
	}
	if (infos->ai_family == AF_INET) {
		ip->v4 = *((struct sockaddr_in *) infos->ai_addr);
	} else if (infos->ai_family == AF_INET6) {
		ip->v6 = *((struct sockaddr_in6 *) infos->ai_addr);
	} else {
		freeaddrinfo(infos);
		return -1;
	}
	freeaddrinfo(infos);
	return 0;
}
